import React from 'react'
import { IonSpinner } from '@ionic/react'

const Loader = () => {
  const style = {
    width: '100%',
    margin: '100px auto'
  }
  return <IonSpinner {...{ style, name: 'bubbles' }} />
}

export default Loader
