import React from 'react'
import { useIonViewDidEnter } from '@ionic/react'
const withIonViewDidEnter = (input) => (Component) => {
  const WrappedComponent = (props) => {
    useIonViewDidEnter(() => {
      input()
    })

    return <Component {...props} />
  }

  return WrappedComponent
}

export default withIonViewDidEnter
