import { connect } from 'react-redux-hoc'
import { compose } from 'recompose'
import withDispatchOnUpdate from '@surglogs/with-dispatch-on-update'

import { shouldLoadUser, getUser } from '../../selectors/user'
import { loadUser } from '../../actions/user'
export default compose(
  connect({
    user: getUser
  }),
  withDispatchOnUpdate({
    action: loadUser,
    condition: shouldLoadUser
  })
)
