import { LOAD_USER } from '../constants/UserActionTypes'
import * as Api from '../lib/api'
export const loadUser = () => ({
  type: LOAD_USER,
  payload: Api.get('/login')
})
