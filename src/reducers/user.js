import typeToReducer from 'type-to-reducer'
import { PENDING, REJECTED, FULFILLED } from 'redux-promise-middleware'
import { assoc } from 'ramda'

import { LOAD_USER } from '../constants/UserActionTypes'

const initialState = {
  user: null,
  pending: false
}

export default typeToReducer(
  {
    [LOAD_USER]: {
      [PENDING](state) {
        return assoc('pending', true, state)
      },
      [FULFILLED](state, { payload }) {
        const { data: user } = payload
        const pending = false
        return { ...state, ...user, pending }
      },
      [REJECTED](state) {
        return assoc('pending', false, state)
      }
    }
  },
  initialState
)
