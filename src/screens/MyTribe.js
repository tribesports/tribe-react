import React from 'react'
import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react'
import { compose, branch, renderComponent } from 'recompose'
import withUser from '../components/hoc/withUser'
import withIonViewDidEnter from '../components/hoc/withIonViewDidEnter'
import './MyTribe.css'
import Loader from '../components/Loader'

const MyTribe = ({ user }) => {
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Tribe</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">{user.username}</IonTitle>
          </IonToolbar>
        </IonHeader>
      </IonContent>
    </IonPage>
  )
}

export default compose(
  withUser,
  branch(({ user }) => {
    return !user
  }, renderComponent(Loader)),
  withIonViewDidEnter(() => {
    console.log('withIonViewDidEnter')
  })
)(MyTribe)
