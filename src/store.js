import { createStore, applyMiddleware } from 'redux'
import { routerMiddleware } from 'connected-react-router'
import thunk from 'redux-thunk'
import promiseMiddleware from 'redux-promise-middleware'
import { createLogger } from 'redux-logger'

import createRootReducer from './reducers'
import history from './history'

const middleware = [routerMiddleware(history), thunk, promiseMiddleware(), createLogger()]

const store = createStore(createRootReducer(history), applyMiddleware(...middleware))

export default store
