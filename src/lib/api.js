import axios from 'axios'

const access_token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiI1ZGM5NmY5NDM0MDc4NjBiNzFmMDRkMTkifQ.8BP0wl7uZRGkJ1R1OwLQ5UcNw0t_OTrbuYfEsJ5FXRw' //&appVersion=1.13.2
// import config from '../config'
const createGetQuery = (obj) => {
  let str = ''
  for (var key in obj) {
    if (str !== '') {
      str += '&'
    }
    str += key + '=' + encodeURIComponent(obj[key])
  }
  return str
}
const get = (url, params = {}) => {
  const data = {
    ...params,
    access_token
  }
  // console.log('[API]', url, data)
  url = 'https://api.tribesportsapp.com' + url + '?' + createGetQuery(data)
  return new Promise((resolve, reject) => {
    axios
      .get(url)
      .then((res) => {
        resolve(res)
      })
      .catch((err) => {
        reject(err)
      })
  })
}

export { get }
