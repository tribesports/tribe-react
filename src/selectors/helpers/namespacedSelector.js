import selector from './selector'

const namespacedSelector = (namespace) => {
  return (key) => {
    return selector(`${namespace}.${key}`)
  }
}

export default namespacedSelector
