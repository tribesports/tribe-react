import { get } from 'lodash'

const selector = (key) => {
  return (state) => {
    return get(state, key)
  }
}

export default selector
