import namespacedSelector from './helpers/namespacedSelector'
import shouldLoadListSelector from './helpers/shouldLoadListSelector'

const selector = namespacedSelector('user')

export const pending = selector('pending')
export const getUser = selector('user')
export const shouldLoadUser = shouldLoadListSelector(getUser, pending)
