# Tribe app React assigment

## Getting Started

This project uses Ionic React 5.0. Follow this guide to get you up and running https://ionicframework.com/docs/installation/cli

### Installing

```
npm i
```

## Assignment
The goal of this assignment is to figure out whether there is a fit for us to work together. 

Your task is to create small, initial part of the Tribe app called MyTribe. Good idea is to download the Tribe app if you haven't done so already.

![MyTribe](https://res.cloudinary.com/dm94yuia1/image/upload/v1582649667/web/mytribe1.png)

### MyTribe

MyTribe is the central screen in the app. We call those boxes channels. Users can add, remove and reorganise their channels (which is not in the scope of this assignment).

To get the user channels data, call **/user/self/following** using the Api wrapper (lib/api.js). The Api wrapper automatically adds an access_token. You'll receive an array of objects. Each object represents one channel. The most important property is **customId.** You'll use it to display the channel logo **https://res.cloudinary.com/dm94yuia1/image/upload/c_fill,h_160/$%7BcustomId%7D.png**

Each channel shows information about last, live or upcoming match. To get the match data, call **/user/self/matches.** One match can be displayed on more then one channel. Match objects have several important properties: home.customId, away.customId and tournamentGroupCId (which can be understood as league customId). 

To figure out what match display at what channel you must iterate through channels and matches and check whether channel customId equals either home.customId, away.customId or tournamentGroupCId. 

Other important match properties:

* status - can be either "scheduled", "inprogress", "closed"
* scheduled - date and time of the match
* home and away alias - to show the team aliases
* homeScore and awayScore - do display score

Show a match on the channel that is the closest to now, i.e. if there is a past match that finished yesterday and another one is in 2 days - show the past one. If the past one was a week ago and there is an upcoming (scheduled) one in 5 days - show the upcoming one.

The last part of the assignment is to implement a screen transition to a new page upon a click on a channel. On that new page you fetch matches calling **/matches/search?customId=${customId}&season=2020**

You can but don't have to display the fetched matches. 

Notes: use redux, write custom HOCs, write clean code, think about good naming. Once you're done we'll have a call about the code and why you made the choices you made. 

The selected candidate will carry on working on the app.

This assignment description is not thorough on purpose. I want you to improvise and ask questions.
